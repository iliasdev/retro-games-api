from app import db


game_platform = db.Table('game_platform',
    db.Column('game_id', db.Integer, db.ForeignKey('game.id'), primary_key=True),
    db.Column('platform_id', db.Integer, db.ForeignKey('platform.id'), primary_key=True)
)

class Platform(db.Model):
    __tablename__ = "platform"
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(256), index=True)
    # games = db.relationship("Game", secondary=game_platform)

    def __repr__(self):
        return f"<Platform {self.id}: {self.name}>"


game_publisher = db.Table('game_publisher',
    db.Column('game_id', db.Integer, db.ForeignKey('game.id'), primary_key=True),
    db.Column('publisher_id', db.Integer, db.ForeignKey('publisher.id'), primary_key=True)
)

class Publisher(db.Model):
    __tablename__ = "publisher"
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(256), index=True)
    # games = db.relationship("Game", secondary=game_publisher)

    def __repr__(self):
        return f"<Publisher {self.id}: {self.name}>"


game_genre = db.Table('game_genre',
    db.Column('game_id', db.Integer, db.ForeignKey('game.id'), primary_key=True),
    db.Column('genre_id', db.Integer, db.ForeignKey('genre.id'), primary_key=True)
)

class Genre(db.Model):
    __tablename__ = "genre"
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(256), index=True)
    # games = db.relationship("Game", secondary=game_genre)

    def __repr__(self):
        return f"<Genre {self.id}: {self.name}>"

class Game(db.Model):
    __tablename__ = "game"
    id = db.Column('id', db.Integer, primary_key=True)
    title = db.Column('title', db.String(512), index=True, nullable=False)
    year = db.Column('year', db.String(4), index=True)
    platforms =  db.relationship("Platform", secondary=game_platform, lazy='subquery', backref=db.backref('games', lazy=True))
    publishers = db.relationship("Publisher", secondary=game_publisher, lazy='subquery', backref=db.backref('games', lazy=True))
    genres =  db.relationship("Genre", secondary=game_genre, lazy='subquery',  backref=db.backref('games', lazy=True))

    def __repr__(self):
        return f"<Game {self.id}: {self.title}>"
