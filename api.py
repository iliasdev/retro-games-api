from app import app, db
from app.models import Platform, Game, Platform, Publisher, Genre, game_genre, game_platform, game_publisher

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Game': Game, 'Platform': Platform, 'Publisher':Publisher, 'Genre':Genre,
    'game_genre': game_genre, 'game_platform': game_platform, 'game_publisher': game_publisher}

if __name__ == '__main__':
    app.run()
